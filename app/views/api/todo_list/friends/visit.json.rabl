object @user
node :data do|user|
	{   :email 				            => user.email,
		:firstname		                => user.firstname,
		:lastname		                => user.lastname,
		:username		                => user.username,
		:contact_number                 => user.contact_number,
		:photo_url                      => user.avatar_url,
		:total_points 	                => user.user_total_points,
		:weekly_point 	                => user.weekly_point,
		:point_to_stack                 => user.point_to_stack,
		:perfect_day                    => user.perfect_day,
		#:tasks_completed_in_percentage => user.tasks_completed_in_percentage
		:total_task_count               => user.total_task_count,
		:completed_task_count           => user.completed_task_count
	}
end

child @tasks => :tasks do
  extends("api/todo_list/tasks/show")
end
