object @task
node :data do|task|
	{   :total_points 	                => task.user.user_total_points,
		:weekly_point 	                => task.weekly_point,
		:point_stack                    => task.user.point_to_stack,
		:perfect_day                    => task.user.perfect_day,
		#:tasks_completed_in_percentage => task.user.tasks_completed_in_percentage,
		:total_task_count               => task.user.total_task_count,
		:completed_task_count           => task.user.completed_task_count
	}
end

