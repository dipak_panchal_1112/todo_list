namespace :todo_list do
  desc "Change Status of Old Task"
  task :previous_task_change_status => :environment do
  	@tasks = Task.where("DATE(created_at) = ?", Time.now.to_date - 1).where("status = ?", "active")
  	if @tasks.present?
  		@tasks.each do |task|
  			task.update_attributes(:status => "uncompleted")
  		end
  	end
  end
end