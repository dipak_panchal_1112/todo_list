class Api::TodoList::SessionsController < Api::BaseController

  before_filter :authentication_user_with_authentication_token, :only => [:logout, :change_password]

  def login
    @user = User.authenticate_user_with_auth(params[:email], params[:password])
    if @user.present?
      @authentication_token = @user.authentication_tokens.create(:auth_token => AuthenticationToken.generate_unique_token)
      @user.check_duplicate_device_ids(params[:device_id],@user,params[:device_type])   
      render :file => "api/todo_list/sessions/register"
    else
      render_json({:errors => User.invalid_credentials, :status => 404}.to_json)
    end
  end

  def logout
    @token = AuthenticationToken.current_authentication_token_for_user(@current_user.id,params[:authentication_token]).first
    if @token.present?
      @token.destroy
      render_json({:message => "Logout Successfully!"}.to_json)
    else
      render_json({:errors => "No user found with authentication_token = #{params[:authentication_token]}"}.to_json)
    end
  end

  def register
    @user = User.new(user_params)
    if params[:avatar].present?
      @user.convert(params[:avatar])
    end
    unless @user.save
      render_json({:errors => @user.display_errors, :status => 404}.to_json)
    else
      @authentication_token = @user.authentication_tokens.create(:auth_token => AuthenticationToken.generate_unique_token)
    end
  end

  def change_password
    current_password = params[:user][:current_password]
    new_password = params[:user][:new_password]
    new_password_confirmation = params[:user][:new_password_confirmation]
    if @current_user.valid_password?(current_password)
      if new_password.to_s == new_password_confirmation.to_s
        @current_user.password = new_password
        @current_user.save
        render_json({:message => "Successfully! Done", :status => 200}.to_json)
      else
        render_json({:errors => "Password and Password Confirmation doesn't match", :status => 404}.to_json)
      end
    else
      render_json({:errors => "Current Password is not valid!", :status => 404}.to_json)
    end
  end

  def forget_password
    if params[:email].present?
      @user = User.find_by_email(params[:email])
      if @user.present?
        @user.send_reset_password_instructions
        render_json({:message => "You will receive an email with instructions about how to reset your password in a few minutes.", :status => 200}.to_json)
      else
        render_json({:errors => "No User found with email #{params[:email]}", :status => 404}.to_json)
      end
    else
      render_json({:errors => "Email Address is required", :status => 404}.to_json)
    end
  end

  private

  def user_params
    params.require(:user).permit(:email, :username, :firstname, :lastname, :password, :password_confirmation, :device_type, :device_id,:contact_number)
  end

end