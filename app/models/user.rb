class User < ActiveRecord::Base
  #searchkick
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  
  ## paperclip attechment
  has_attached_file :avatar, :default_url => "assets/no_photo_image.png"

  ##validations
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
  validates_format_of :contact_number, 
                      :with => /\A[0-9]{10,15}\Z/,:maximum =>15,:minimum =>10

  ## Relationships
  has_many :authentication_tokens, dependent: :destroy
  has_many :tasks, dependent: :destroy
  has_many :sent_friend_request, class_name: 'Friend',foreign_key: 'user_one',dependent: :destroy
  has_many :recived_friend_request, class_name: 'Friend',foreign_key: 'user_two',dependent: :destroy
  has_and_belongs_to_many :task_notifications, :class_name => 'Task', :join_table => :notification_details
  ## Scopes
  scope :without_user, lambda{|user| where (user ? ["id != ?", user.id] : {}) }      
  scope :get_user_device_ids,lambda { |device_id| where("device_id = ?", device_id) }
  
  #validates_confirmation_of :password

  ## Class Methods ##
  class << self
    def authenticate_user_with_auth(email, password)
      return nil unless email.present? or password.present?
      u = User.find_by_email(email)
      (u.present? && u.valid_password?(password))? u : nil
    end

    def invalid_credentials
      "Email or Password is not valid"
    end

    def success_message
      {:message=> "ok", :errorcode => "",:rstatus=>1}
    end

    def search(search_param,search_type,current_user_id)
      if search_type == "contact number"
        where("contact_number = ? and id != ?",search_param,current_user_id)
      elsif search_type == "name"
        where("firstname like ? or lastname like ? and id != ?", "%#{search_param}%","%#{search_param}%",current_user_id)
      elsif search_type == "email"
        where("email like ? and id != ?", "%#{search_param}%",current_user_id)
      else
        []
      end
    end
  end

  def display_errors
    self.errors.full_messages.join(', ')
  end

  def success_message
    {:messages=> "ok", :errorcode => "",:rstatus=>1}
  end

  def user_total_points
    if self.total_points.present?
      return self.total_points
    else
      return 0
    end
  end

  def check_duplicate_device_ids(device_id,user,device_type)
    @users = User.without_user(user).get_user_device_ids(device_id)
    if @users.present?
      @users.update_all({:device_id => nil})
    end
    user.device_id = device_id
    user.device_type = device_type
    user.save 
  end

  def weekly_point
    start_date = Time.now.end_of_week - 7.day
    end_date = Time.now.end_of_week
    begin
    total_point = self.tasks.where("updated_at > ? AND updated_at < ? and status not in (?)", start_date, end_date,["pending","reject"]).sum(:point)
    rescue
      total_point = 0
    end
  end

  def point_to_stack
    stack_point = self.tasks.where("DATE(created_at) = ?", Time.now.to_date).where("status = ?","active").sum("point")
    # keyword = self.tasks.where("DATE(created_at) = ?", Time.now.to_date).where("task_type = ? and status != ?", "keyword", "completed").length
    # custom = self.tasks.where("DATE(created_at) = ?", Time.now.to_date).where("task_type = ? and status != ?", "custom", "completed").length
    # keyword_point = keyword * 2
    # custom_point = custom * 1
    # stack_point = keyword_point + custom_point
  end

  def perfect_day_count
    index = 0
    if self.tasks.present?
      i = 1
      while (1)
        tasks_count = self.tasks.where("DATE(created_at) = ? and status not in (?)", Time.now.to_date - i,["pending","reject"]).count
        tasks_completed_count = self.tasks.where("DATE(created_at) = ?", Time.now.to_date - i.days).where("status = ?", "completed").count
        if tasks_count.to_i == tasks_completed_count.to_i && tasks_count.to_i != 0
          index = index + 1
        else
          break
        end
        i = i + 1
      end
    end
    return index
  end

  def total_task_count
    self.tasks.where("DATE(created_at) = ? and status not in (?)", Time.now.to_date,["pending","reject"]).count
  end

  def completed_task_count
    self.tasks.where("DATE(created_at) = ? and status = ?", Time.now.to_date,"completed").count
  end

  # def tasks_completed_in_percentage
  #   tasks_count = self.tasks.where("DATE(created_at) = ?", Time.now.to_date).count
  #   tasks_completed_count = self.tasks.where("DATE(created_at) = ?", Time.now.to_date).where("status = ?", "completed").count
  #   result = tasks_completed_count.to_f / tasks_count
  #   if result.to_s.downcase == "nan"
  #     return 0
  #   else
  #     return result
  #   end
  # end

  def convert(photo)
    cid = URI.unescape(photo)
    filename = "photo#{Time.now.to_i}"
    file = File.open("#{Rails.root.to_s}/tmp/#{filename}.png","wb")
    temp2 = Base64.decode64(cid)
    file.write(temp2)
    file.close
    f = File.open("#{Rails.root.to_s}/tmp/#{filename}.png")
    self.avatar = f
    f.close
    File.delete("#{Rails.root.to_s}/tmp/#{filename}.png")
  end

  def has_android_device?
    device_type.downcase == "android"
  end

  def has_iphone_device?
    device_type.downcase == "iphone"
  end

  def avatar_url
    "#{DOMAIN_CONFIG}/#{self.avatar.url}"
  end

  def friends(status)
    if status == "all"
      friend_ids = self.sent_friend_request.pluck("user_two").concat(self.recived_friend_request.pluck("user_one"))
    elsif status == "pending"
      friend_ids = self.sent_friend_request.with_status('pending').pluck("user_two").concat(self.recived_friend_request.with_status('pending').pluck("user_one"))
    elsif status == "confirm"
      friend_ids = self.sent_friend_request.with_status('confirm').pluck("user_two").concat(self.recived_friend_request.with_status('confirm').pluck("user_one"))
    else
      friend_ids = []
    end
    return User.where(id: friend_ids)
  end

  def request_status(current_user_id)
    id = self.id
    friends_request = Friend.where(user_one: [current_user_id,id],user_two: [current_user_id,id]).last
    if friends_request.present?
      if friends_request.status == "pending" || friends_request.status == "block"
        if friends_request.user_one == current_user_id
          status = "sent " + friends_request.status
        else
          status = "received " + friends_request.status
        end
      else
        status = friends_request.status
      end
    else
      status = "unknown"
    end
    return status
  end

  def send_notification(msg)
    if self.device_id?
      if self.has_android_device?
        GCM.key = ""
        GCM.send_notification(self.device_id, {:message => msg}, :collapse_key => "Task", :time_to_live => 3600)
        puts "android"
      elsif self.has_iphone_device?         
        APNS.port ='2195'
        APNS.pem  = "#{Rails.root}/config/pushCert.pem"
        APNS.host = 'gateway.sandbox.push.apple.com'
        APNS.send_notification(self.device_id, :alert => msg, :badge => 1, :sound => 'default')
        puts "iphone"     
      end
    end
  end
end
