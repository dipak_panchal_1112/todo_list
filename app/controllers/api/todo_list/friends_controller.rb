class Api::TodoList::FriendsController < Api::BaseController
	before_filter :authentication_user_with_authentication_token

	def create
		@friend = Friend.new(friend_params)
		@friend.user_one = @current_user.id
		if @friend.save
			render_json({:message => "Friend Request sent successfully", :status => 200}.to_json)
		else
			render_json({:errors => @friend.display_errors, :status => 404}.to_json)
		end
	end

	def status_change
		@friend = Friend.where(user_one: [params[:sender_id],@current_user.id],user_two: [@current_user.id,params[:sender_id]]).last
		if @friend.present?
			if params[:status].present?
				@friend.status = params[:status]
				if @friend.save
					render_json({:message => "Friend request #{params[:status]} successfully", :status => 200}.to_json)
				else
					render_json({:message => "error", :status => 200}.to_json)
				end
			else
				render_json({:errors => "Status can't be blank", :status => 404}.to_json)			
			end
		else
			render_json({:errors => "No friend request found", :status => 404}.to_json)
		end
	end

	def contact_list
		if params[:emails].present? && params[:names].present?
			emails = params[:emails].split(",")
			names = params[:names].split(",")
			users_emails = User.all.pluck("email")
			friends = @current_user.friends("all").pluck("email")
			unregistered = emails - users_emails
			registered = users_emails - friends
			@list = Array.new
			emails.zip(names).each do |email,name|				
				if unregistered.include?(email)
					status = "unregistered"
				elsif friends.include?(email)
					user = User.find_by(email: email)
					firstname = user.firstname
					perfect_day = user.perfect_day
					id = user.id
					status = user.request_status(current_user.id)
				elsif registered.include?(email)
					status = "registered"
					user = User.find_by(email: email)
					firstname = user.firstname
					perfect_day = user.perfect_day
					id = user.id
				end
				@list << [id.present? ? id : 0,firstname.present? ? firstname : name,email,perfect_day.present? ? perfect_day : 0,status]
			end
		else
			render_json({:errors => "emails and names parameters are required", :status => 404}.to_json)
		end
	end

	def index
		@users = @current_user.friends("all").order("perfect_day,firstname NULLS LAST")
		unless @users.present?
			render_json({:errors => "no friend request sent or recived", :status => 404}.to_json)
		end
	end

	def popular_task
		@data = current_user.friends("confirm").joins(:tasks).where("tasks.status not in (?)",["pending","reject"]).select("users.id as user_id,users.firstname as name,users.email as email,users.perfect_day as perfect_day,task_name,task_type").group("users.id,task_type,name,email,perfect_day,task_name").having("COUNT(*) > 1").group_by(&:task_name)
	end

	def delete
		@friend = Friend.where(user_one: [params[:sender_id],@current_user.id],user_two: [@current_user.id,params[:sender_id]]).last
		if @friend.present?
			@friend.destroy
			render_json({:message => "Friend request deleted successfully", :status => 200}.to_json)
		else
			render_json({:errors => "No friend request found", :status => 404}.to_json)
		end		
	end

	def invite
		if params[:email].present? 
			InviteFriend.mail_to_invite_friend(params[:email],current_user).deliver
			render_json({:message => "Invitation Sent successfully", :status => 200}.to_json)
		else
			render_json({:errors => "Email is required", :status => 404}.to_json)
		end
	end

	def visit
		if params[:friend_id].present?
			@user = User.find(params[:friend_id])
			if @user.present?
				@tasks = @user.tasks.today_task.public_task.where("status not in (?)",["pending","reject"])
			else
				render_json({:errors => "No User found with id #{params[:friend_id]}", :status => 404}.to_json)	
			end
		else
			render_json({:errors => "friend_id is required", :status => 404}.to_json)
		end
	end

	private
	def friend_params
		params.require(:friend).permit(:user_two)
	end
end
