class AddColumnFavoriteToTask < ActiveRecord::Migration
  def change
    add_column :tasks, :favorite, :boolean, default: false
  end
end
