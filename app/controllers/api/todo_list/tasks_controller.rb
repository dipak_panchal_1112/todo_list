class Api::TodoList::TasksController < Api::BaseController

  before_filter :authentication_user_with_authentication_token

  def create
    if params[:task].present?
      if params[:task][:user_id].present?
        @user = User.find(params[:task][:user_id])
        today_task = @user.tasks.where("DATE(created_at) = ? and status not in (?)", Time.now.to_date,["pending","reject"])
      else
        today_task = @current_user.tasks.where("DATE(created_at) = ? and status not in (?)", Time.now.to_date,["pending","reject"])
      end
      if today_task.present?
        count = today_task.length
      else
        count = 0
      end
      if count < 6
        if params[:task][:user_id].present?
          @task = @user.tasks.build(task_params)
        else
          @task = @current_user.tasks.build(task_params)
        end
        @task.owner_id = @current_user.id
        if @task.valid?
          @task.save
          #render_json({:message => "Successfully Created Task.", :status => 200}.to_json)
        else
          render_json({:errors => @task.display_errors, :status => 404}.to_json)
        end
      else
        render_json({:errors => "You can not add more than 6 task per day.", :status => 404}.to_json)
      end
    else
      render_json({:errors => "Please provide task parameters for creating task.", :status => 404}.to_json)
    end
  end

  def index
    active_true = @current_user.tasks.active_true.today_task
    active_false = @current_user.tasks.active_false.today_task
    completed_true = @current_user.tasks.completed_true.today_task
    completed_false = @current_user.tasks.completed_false.today_task
    active_true_false = active_true.concat(active_false)
    completed_true_false = completed_true.concat(completed_false)
    #@tasks = active_true_false.concat(completed_true_false)
    #@tasks = @tasks.uniq
    @tasks = @current_user.tasks.where("DATE(created_at) = ? and status not in (?)", Time.now.to_date,["pending","reject"]).order(status: :asc, favorite: :desc)
  end

  def show
    @task = @current_user.tasks.find(params[:id])
  end

  def destroy_task
    @task = @current_user.tasks.find(params[:id])
    if @task.present?
      @task.destroy
      render_json({:message => "Task is successfully deleted.", :status => 200}.to_json)
    else
      render_json({:errors => "Task is not present.", :status => 404}.to_json)
    end
  end

  def edit_task
    @task = @current_user.tasks.find(params[:id])
    if @task.present?
      @task.update_attributes(task_params)
      render_json({:message => "Task is successfully updated.", :status => 200}.to_json)
    else
      render_json({:errors => "Task is not present.", :status => 404}.to_json)
    end

  end

  def completed_status
    @task = @current_user.tasks.find(params[:id])
    if @task.present?
      if @task.is_active?        
        if @task.is_keyword?
          point = 2
        elsif @task.is_custom?
          point = 1
        else
          point = 0
        end
        total_point = @task.point + point
        @task.update_attributes(:status => "completed", :point => total_point)
        if @current_user.total_points.present?
          total_user_points = @current_user.total_points + point
        else
          total_user_points = 0 + point
        end
        @current_user.update_attributes(:total_points => total_user_points)
        @task.send_completed_task_notification
        #render_json({:message => "Status is successfully completed", :status => 200}.to_json)
      else
        render_json({:errors => "Status is not active", :status => 404}.to_json)
      end
    else
      render_json({:errors => "Task is not present.", :status => 404}.to_json)
    end
  end

  def popular_tasks
    @tasks = Task.where("status not in (?)",["pending","reject"]).select("task_name,task_type, COUNT(*) AS cnt").group("task_name,task_type").having("COUNT(*) > 1")
  end

  def recent_tasks
    @tasks = @current_user.tasks.where("status not in (?)",["pending","reject"]).order("created_at DESC").limit(10)
    render :file => "api/todo_list/tasks/index"
  end

  def accept_or_reject
    @task = @current_user.tasks.find(params[:id])
    if @task.present?
      if @task.is_pending?
        if params[:status].present?
          @task.update_attributes(status: params[:status])
          render_json({:errors => "Task successfully #{@task.is_active? ? "Accepted" : "Rejected"}.", :status => 404}.to_json)
        else
          render_json({:errors => "Parameter Status required.", :status => 404}.to_json)
        end
      else
        render_json({:errors => "Task is not pending.", :status => 404}.to_json)
      end
    else
      render_json({:errors => "Task is not present.", :status => 404}.to_json)
    end
  end

  def notify
    @task = Task.find(params[:id])
    if @task.present?
      @task.notification_recivers << @current_user
      render_json({:errors => "You will recive notification on completion of task #{@task.task_name}.", :status => 404}.to_json)
    else
      render_json({:errors => "Task is not present.", :status => 404}.to_json)
    end
  end


  private

  def task_params
    params.require(:task).permit(:favorite, :user_id, :task_name, :status, :point, :amount, :tnumber, :note, :category, :task_type, :inner_task)
  end

end